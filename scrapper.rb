require 'http'
require 'nokogiri'
require 'uri'
require 'byebug'

class Scrapper
  attr_reader :params, :base_url
  attr_accessor :uri

  def initialize(site)
    @uri = URI(site)
    @base_url = URI.join(@uri, '/').to_s
    @params = @uri.path.split('/').last.split(',')
  end

  def year
    @params[1]
  end

  def page(id)
    @params[4] = id.to_s
    path = @uri.path.split('/')[0...-1].join('/')
    path << '/'
    @uri = URI.join(@base_url, path, @params.join(','))
  end

  def fetch
    url = @uri.to_s
    body = HTTP.get(url).to_s
    @parser = Nokogiri::HTML(body)
  end

  def scrap_years
    @parser.css('div.lata > a').map(&:text)
  end

  def scrap_reports
    @parser.css('a.th').map { |node| node[:href] }
  end

  def scrap_pages
    @parser.css('div.notowania > div.stronicowanie > b').map(&:text)
  end

  def scrap_attachments
    @parser.css('table.nDokument > tbody > tr.dane a').map { |node| node[:href] }
  end
end
