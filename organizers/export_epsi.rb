require 'interactor'
require_relative '../interactors/execute_request'
require_relative '../interactors/scrap_last_page'
require_relative '../interactors/validate_year'
require_relative '../interactors/download_attachments'
require_relative '../interactors/scrap_reports'
require_relative '../interactors/scrap_attachments'


class ExportEpsi
  include Interactor::Organizer

  organize ExecuteRequest,
           ValidateYear,
           ScrapLastPage,
           ScrapReports,
           ScrapAttachments,
           DownloadAttachments
end
