require 'interactor'
require 'forwardable'
require 'down'
require 'uri'

class DownloadAttachments
  extend Forwardable
  include Interactor

  def_delegators :context, :attachments, :scrapper

  def call
    Dir.mkdir('./output') unless File.exists?('./output')

    attachments.each do |attachment|
      url = URI.join(scrapper.base_url, attachment, destination: './output')
      Down.download(url)
      sleep(1)
    end
  end
end