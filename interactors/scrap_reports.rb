require 'interactor'

class ScrapReports
  extend Forwardable
  include Interactor

  def_delegators :context, :scrapper, :last_page

  def call
    context.reports = Array.new

    (1..last_page).each do |page|
      scrapper.page(page)
      scrapper.fetch
      context.reports += scrapper.scrap_reports
      sleep(3)
    end
  end
end