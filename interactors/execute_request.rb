require 'interactor'
require 'forwardable'

class ExecuteRequest
  extend Forwardable
  include Interactor

  def_delegators :context, :scrapper

  def call
    scrapper.fetch
    context.years = scrapper.scrap_years
    context.current_year = scrapper.year
  end
end
