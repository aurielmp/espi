require 'interactor'
require 'forwardable'

class ValidateYear
  extend Forwardable
  include Interactor

  def_delegators :context, :years, :current_year

  def call
    context.fail!(error: 'Specified year does not exist on website.') unless years.include?(current_year)
  end
end
