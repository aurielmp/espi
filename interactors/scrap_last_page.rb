require 'interactor'
require 'forwardable'
require_relative '../scrapper'

class ScrapLastPage
  extend Forwardable
  include Interactor

  def_delegators :context, :scrapper

  def call
    pages = scrapper.scrap_pages
    context.last_page = pages.empty? ? 1 : pages.last.to_i
  end
end
