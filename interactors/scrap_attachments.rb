require 'interactor'
require 'forwardable'
require 'uri'
require 'byebug'

class ScrapAttachments
  extend Forwardable
  include Interactor

  def_delegators :context, :scrapper, :reports

  def call
    context.attachments = Array.new

    reports.each do |report|
      scrapper.uri = URI(report)
      scrapper.fetch
      context.attachments += scrapper.scrap_attachments
      sleep(2)
    end
  end
end
